# Summercourse CI/CD

Welcome to the codecentric summercourse for Continuous Integration and Deployment!
Today we will learn how to set up a basic pipeline in Gitlab. 

After today you will know the following things:

- How to create CI jobs
- How to chain CI jobs together
- How to deploy a web application to Amazon S3 automatically
- How to integrate branching strategies in your pipeline
- How to use secrets in your pipeline

## Prerequisites

There are some things you need in order to get started:

- [x] You should fork this repository to your own Gitlab account.
- [x] You should have a favorite yaml editor open
- [x] Recommendations: IntelliJ, Sublime Text 3 or VsCode
- [x] You can also use the Gitlab IDE in your fork

That's it! Everything else is already taken care of. 

## The Basics

Before you can get started, we'll share some basic information with you. 
After that you can get your hands as dirty as you want!

### Defining a job
To define a job, you should open the `.gitlab-ci.yml` file in your forked repository.

You should edit it to look like this:

```yml
hello_world:
  script:
  - echo "Hello world!"
```

Commit and push this change. 
You can now see a pipeline running under de CI/CD -> Pipeline in your Gitlab repository!
When you click on the step you have defined, you will see a text; Hello world!

### Images

When defining a job, you can define a Docker image to have the job run in. 
This way, you can build your own Docker images with custom tooling needed for you project.

We already maintain two Docker images with the appropriate tooling for this project. These are:

- `morlack/maven-8-aws-cli-docker`: Tooling for our backend builds
    - aws cli
    - Maven 3.x
    - Java 8
    - Docker (in Docker)
- `morlack/node-chromium`: Tooling for our frontend builds
    - Node 9
    - Chromium Headless
    
To use one of these, edit your `.gitlab-ci.yml` to look like this:

```yml
aws_version:
  image: morlack/node-chromium
  script:
  - node --version
```

After committing and pushing, a pipeline will have been created with this job. 
The output should print the Node version of the Docker container.

### Runners
Last but not least, Runners are needed for your pipeline. 
The tooling for your builds will have to be executed somewhere!
Fortunately for us, this is no problem at all since Gitlab gives us 2000 minutes of CI execution per month for free.
This is more than enough for this summercourse.

## Ready, Set, Go!
You are now ready to begin the summercourse workshop! 
We have defined multiple chapters using Markdown files in the repository's main folder. 
After finishing a chapter, compare your result to ours. 
You can use `git diff result/chapter-X` for this.

Feel free to hail us to discuss things or to ask for feedback! Good Luck!